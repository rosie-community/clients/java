# java

A Java binding for librosie is hosted [here](https://github.com/antoniomacri/rosie-pattern-language-java).  Please direct issues and discussion there.

Many thanks to [Antonio Macrì](https://github.com/antoniomacri)!
